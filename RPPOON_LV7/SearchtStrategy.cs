﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV7
{
    abstract class SearchStrategy
    {
        public abstract int Search(double number, double[] arr);
    }
}
