﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] myArray = new double[] { 25, 10, -273, -16, 20, 13, 64, 87, 57, 99 };
            NumberSequence numberSequence = new NumberSequence(myArray);
            BubbleSort bubbleSort = new BubbleSort();
            numberSequence.SetSortStrategy(bubbleSort);

            Console.WriteLine("Niz koji sortiramo: \n" + numberSequence.ToString());
            numberSequence.Sort();
            Console.WriteLine("Niz nakon sortiranja:\n" + numberSequence.ToString());

        }
    }
}
